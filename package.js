Package.describe({
  name: 'inspinia-less',
  summary: 'less styles of inspinia theme',
  version: '0.0.3',
});

Package.onUse((api) => {
  api.versionsFrom('1.5');
  api.use('less');
  api.addFiles([
    'imports/animate.import.less',
    'imports/badgets_labels.import.less',
    'imports/base.import.less',
    'imports/buttons.import.less',
    'imports/chat.import.less',
    'imports/checkbox.import.less',
    'imports/clock-picker.import.less',
    'imports/color-picker.import.less',
    'imports/custom.import.less',
    'imports/data-table.import.less',
    'imports/date-picker.import.less',
    'imports/duallistbox.import.less',
    'imports/elements.import.less',
    'imports/fileinput.import.less',
    'imports/fix.import.less',
    'imports/landing.import.less',
    'imports/md-skin.import.less',
    'imports/media.import.less',
    'imports/metismenu.import.less',
    'imports/mixins.import.less',
    'imports/navigation.import.less',
    'imports/pages.import.less',
    'imports/rtl.import.less',
    'imports/sidebar.import.less',
    'imports/skins.import.less',
    'imports/spinners.import.less',
    'imports/text-spinners.import.less',
    'imports/theme-config.import.less',
    'imports/top_navigation.import.less',
    'imports/typeahead.import.less',
    'imports/typography.import.less',
    'imports/variables.import.less',
    'styles.less',
  ], 'client', { isImport: true });
});
